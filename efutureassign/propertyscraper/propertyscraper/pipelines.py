# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

class PropertyscraperPipeline:
    def process_item(self, item, spider):
        item['title'] = item['title'][0]
        item['distric'] = item['distric'][0].split(',')[0]
        item['facilites'] = item['facilites'][0]
        item['price'] = float(item['price'][0])
        print(item)
        item.save()
        return item