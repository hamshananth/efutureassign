import scrapy
from ..items import PropertyscraperItem
from scrapapp.models import Property
from django.contrib.auth.models import User

class PropertySpider(scrapy.Spider):
    name = 'property'
    start_urls = ['https://ikman.lk/en/ads/sri-lanka/houses-for-sale']

    def __init__(self, *args, **kwargs):
        self.user_id = kwargs.get('auth_user_id')


    def parse(self, response):
        for props in response.css('li.normal--2QYVk.gtm-normal-ad'):
            item = PropertyscraperItem()
            item['title'] = props.css('h2.heading--2eONR.heading-2--1OnX8.title--3yncE.block--3v-Ow::text').get(),
            item['distric'] = props.css('div.description--2-ez3::text').get(),
            item['facilites'] = props.css('div.content--3JNQz div div::text').get(),
            item['price'] = props.css('div.price--3SnqI.color--t0tGX span::text').get().replace('Rs ', '').replace(',',''),
            item['addedby'] = User.objects.get(id=self.user_id)

            yield(item)
