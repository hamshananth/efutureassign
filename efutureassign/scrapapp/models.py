from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class  Property(models.Model):
    title = models.CharField(max_length=200)
    facilites = models.CharField(max_length=150)
    price = models.FloatField(help_text='in LKR')
    distric = models.CharField(max_length=50, blank=True)
    datecreated = models.DateTimeField(auto_now_add=True)
    addedby = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.id} - Added By {self.addedby.id}"

