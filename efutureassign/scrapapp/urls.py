from django.urls import path

from .views import (dashboard_view, scrap_view) 

app_name = 'scrapapp'

urlpatterns = [
    path('', dashboard_view, name = 'dashboard'),
    path('managedata/', scrap_view, name = 'managedata'),

]