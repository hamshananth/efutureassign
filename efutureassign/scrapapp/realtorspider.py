import scrapy

class ReaaltorSpider(scrapy.Spider):
    name = 'whisky'
    start_urls = ['https://ikman.lk/en/ads/sri-lanka/houses-for-sale']

    def parse(self, response=None):
        data = response.css("li.normal--2QYVk.gtm-normal-ad")

        for line in data:
            title = line.css('div.description--2-ez3::text').extract_first()
            print(title)
