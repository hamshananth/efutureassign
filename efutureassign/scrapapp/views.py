from django.http import JsonResponse, response
from django.shortcuts import render

from urllib.parse import urlparse

from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST, require_http_methods
from django.views.decorators.csrf import csrf_exempt

from scrapyd_api import ScrapydAPI


from .models import Property

from django.db import connection

# Create your views here.
@login_required
def dashboard_view(request):
    propertys = Property.objects.filter(addedby=request.user.id)

    cursor = connection.cursor()
    cursor.execute('''SELECT distric , avg(price)
        FROM scrapapp_property
        GROUP BY distric''')
    avgprice_with_distric = cursor.fetchall()

    cursor = connection.cursor()
    cursor.execute('''SELECT distric , count(price)
        FROM scrapapp_property
        GROUP BY distric''')
    num_of_props_per_distric = cursor.fetchall()

    return render(request, 'scrapapp/dashboard.html', {'propertys': propertys, 'awd':avgprice_with_distric, 'npd':  num_of_props_per_distric})

@csrf_exempt
def scrap_view(request):
    Property.objects.filter(addedby=request.user.id).delete()

    # connect scrapyd service
    scrapyd = ScrapydAPI('http://localhost:6800')

    # Initiate Scraping
    task = scrapyd.schedule('default', 'property', auth_user_id= request.user.id)

    return render(request, 'scrapapp/scrappage.html', {'task_id': task, 'status': 'started' })
    

